import React from 'react'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import Home from '../pages/Home'
import LayoutLanding from '../widgets/LayoutLanding'
import LayoutDashboard from '../widgets/LayoutDashboard'
import Dashboard from '../pages/Dashboard'
import Detail from '../pages/Detail'
import About from '../pages/About'
import Profile from '../pages/Profile'
import Login from '../pages/Login'
import Register from '../pages/Register'
import Cookies from 'js-cookie'
import ListData from '../pages/ListData'
import CreateData from '../pages/CreateData'
import { GlobalProvider } from '../context/GlobalContext'

const RouteComponent = () => {

  const LoginRoute = (props) => {
    if(Cookies.get('token') === undefined){
      return props.children
    }else if(Cookies.get('token') !== undefined){
      return <Navigate to={'/'} />
    }
  }

  const DashboardRoute = (props) => {
    if(Cookies.get('token') === undefined){
      return <Navigate to={'/login'} />
    }else if(Cookies.get('token') !== undefined){
      return props.children
    }
  }

  return (
    <BrowserRouter>
      <GlobalProvider>
        <Routes>

          <Route path='/' element={
            <LayoutLanding>
              <Home/>
            </LayoutLanding>
          } />

          <Route path='/job-vacancy' element={
            <LayoutLanding>
                <Dashboard/>
            </LayoutLanding>
          } />

          <Route path='/detail/:id' element={
            <LayoutLanding>
                <Detail />
            </LayoutLanding>
          } />

          <Route path='/profile' element={
            <LayoutLanding>
                <Profile />
            </LayoutLanding>
          } />

          <Route path='/about' element={
            <LayoutLanding>
              <About />
            </LayoutLanding>
          } />

          <Route path='/dashboard' element={
            <DashboardRoute>
              <LayoutDashboard>
                <Dashboard/>
            </LayoutDashboard>
            </DashboardRoute>
            
          } />

          <Route path='/dashboard/list-job-vacancy' element={
            <DashboardRoute>
              <LayoutDashboard>
                <ListData/>
              </LayoutDashboard>
            </DashboardRoute>
            
          } />

          <Route path='/dashboard/create-job-vacancy' element={
            <DashboardRoute>
              <LayoutDashboard>
                <CreateData/>
              </LayoutDashboard>
            </DashboardRoute>
            
          } />

          <Route path='/login' element={
            <LoginRoute>
              <Login />
            </LoginRoute>
          } />

          <Route path='/register' element={
            <LoginRoute>
              <Register />
            </LoginRoute>
          } />

        </Routes>
      </GlobalProvider>
    </BrowserRouter>
  )
}

export default RouteComponent