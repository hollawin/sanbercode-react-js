import React from 'react'
import Cookies from 'js-cookie';
import axios from 'axios';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';


const Register = () => {

    const navigate = useNavigate(); 

    const [input, setInput] = useState({
        name : " ",
        email : " ",
        password : " "
    })

    const handleChange = (e) => setInput({...input, [e.target.name] : e.target.value})

    const handleSubmit = (e) => {
        e.preventDefault()
        // console.log(input)
    
        let {name, email, password} = input
    
        axios.post('https://dev-example.sanbercloud.com/api/register', {name, email, password})
        .then((e) => {
          let data = e.data
          
          let {token} = data
          console.log(token)
          Cookies.set('token', token, {expires : 1})
          navigate('/')
    
        })
    
      }
    
  return (
    <div className='mx-auto my-10 w-1/4'>
        <h1 className='text-center text-4xl font-bold mb-10'>Register</h1>
        <form onSubmit={handleSubmit}>
            <div className="mb-6">
                <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Name</label>
                <input type="text" name="name" id="name" onChange={handleChange} value={input.name} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required=""/>
            </div>
            <div className="mb-6">
                <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Email</label>
                <input type="email" name="email" id="email" onChange={handleChange} value={input.email} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required=""/>
            </div>
            <div className="mb-6">
                <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Password</label>
                <input type="password" name="password" id="password" onChange={handleChange} value={input.password} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required=""/>
            </div>
            <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
        </form>
        
        <div className=" text-center mb-6 mt-3">
            <p>
                Sudah punya akun? 
                <Link to="/login" className='text-blue-700'> Login Di sini!</Link>
            </p>
        </div>
    </div>

  )
}

export default Register