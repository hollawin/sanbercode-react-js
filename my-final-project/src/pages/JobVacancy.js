import React from 'react'

const JobVacancy = () => {
  const [data, setData] = useState(null)

  useEffect(() => {
    axios.get('https://dev-example.sanbercloud.com/api/job-vacancy')
    .then((res) => {
      // console.log(res.data.data)
      setData(res.data.data)
    })

  }, [])

  console.log(data)

  const handleText = (e) => {
    if(e === null){
      return ""
    } else{
      return e.slice(0, 100) + ". . ."
    }
  }

  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      let separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }

  return (
    <div>
      <h1 className='font-bold text-xl  text-center mt-10 mb-10'>Lowongan yang tersedia</h1>
      <div className='flex flex-wrap gap-5 justify-center'>
      {
        data !== null && data.map((res) => {
          return (
            <Link to={`/detail/${res.id}`} key={res.id} className="w-full flex flex-col max-w-xs p-6 overflow-hidden bg-white shadow-lg rounded-xl dark:bg-gray-800">
            <div className="flex flex-col items-center justify-between md:flex-row">
              <div className="flex items-center justify-start flex-grow w-full">
                <span className="relative block">
                  <img alt="profil" src={res.company_image_url} className="mx-auto object-cover rounded-full h-10 w-10 " />
                </span>
                <div className="flex flex-col items-start ml-4">
                  <span className="text-gray-700 dark:text-white">
                    {res.company_name}
                  </span>
                  <span className="text-sm font-light text-gray-400 dark:text-gray-300">
                  {res.company_city}
                  </span>
                </div>
              </div>
              <span className="flex-none px-3 py-1 text-sm text-gray-500">
                  {res.job_type}
                </span>
            </div>
            <p className="mt-4 mb-2 text-lg text-gray-800 dark:text-white">
              {res.title}
            </p>
            <p className="grow text-sm font-normal text-gray-400">
              {handleText(res.job_description)}
            </p>
            <div className="flex items-center justify-between p-2 my-6 bg-blue-100 rounded">
              <div className="flex items-start justify-between w-full">
                <p className="flex-grow w-full text-2xl text-gray-700">
                  <span className="font-light text-gray-400 text-lg">
                    Rp. 
                  </span>
                    {formatRupiah(res.salary_min + "")}
                  <span className="text-sm font-light text-gray-400">
                    /Month
                  </span>
                </p>
                <span className="flex-none px-2 py-1 text-sm text-indigo-500 border border-indigo-500">
                  {res.job_tenure}
                </span>
              </div>
            </div>
          </Link>
          )
        })
      }  
      </div>
    </div>
  )
}

export default JobVacancy