import React from 'react'
import Cookies from 'js-cookie'
import { useState, useEffect } from 'react'

const Profile = () => {
    const [user, setUser] = useState("undefined")

  useEffect(() => {
    if(Cookies !== undefined) {
      setUser(JSON.parse(Cookies.get('user')))
    }
  }, [])


  return (
    <div className="w-64 bg-white shadow-lg rounded-2xl dark:bg-gray-800">
        <img alt="profil" src="/images/landscape/1.jpg" className="w-full mb-4 rounded-t-lg h-28" />
        <div className="flex flex-col items-center justify-center p-4 -mt-16">
            <a href="#" className="relative block">
            <img alt="profil" src="/images/person/1.jpg" className="mx-auto object-cover rounded-full h-16 w-16 " />
            </a>
            <p className="mt-2 text-xl font-medium text-gray-800 dark:text-white">
                {user.name}
            </p>
            <p className="flex items-center text-xs text-gray-400">
                {user.email}
            </p>
            <div className="flex items-center justify-between w-full gap-4 mt-8">
            <button type="button" className="py-2 px-4  bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                Edit
            </button>
            </div>
        </div>
    </div>

  )
}

export default Profile