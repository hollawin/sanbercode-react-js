import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const Detail = () => {

  const [data, setData] = useState(null)
   let {id} = useParams()
  // console.log(id)

  useEffect(() => {

    if(id !== undefined){
        axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${id}`)
        .then((res) => {
            // console.log(res.data)
            setData(res.data)
        })
    }

  }, [id])
  
  console.log(data)


  return (
    <div>
    
    <div className="mb-4">
              <div className="w-full p-4 bg-white shadow-lg rounded-2xl dark:bg-gray-700">
                <div className="flex items-center justify-between mb-6">
                  <div className="flex items-center">
                    <span className="relative bg-blue-100 rounded-full">
                        <img alt="profil" src={data?.company_image_url} className="mx-auto object-cover rounded-full h-10 w-10 " />
                    </span>
                    <div className="flex flex-col">
                      <span className="ml-2 font-bold text-black text-md dark:text-white">
                        {data?.company_name}
                      </span>
                      <span className="ml-2 text-sm text-gray-500 dark:text-white">
                        {data?.company_city}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="flex items-center mb-4 space-x-8">
                  <span className="flex items-center px-2 py-1 text-xs font-semibold text-gray-500 bg-gray-200 rounded-md">
                    {data?.job_type}
                  </span>
                  <span className="flex items-center px-2 py-1 text-xs font-semibold text-red-400 bg-white border border-red-400 rounded-md">
                    {data?.job_tenure}
                  </span>
                </div>
                <div className="block m-auto">
                  <div>
                    <span className="inline-block text-sm text-gray-500 dark:text-gray-100">
                        {data?.job_description} 
                    </span>
                  </div>
                </div>
                <span className="flex items-center px-2 py-1 mt-4 text-sm font-semibold text-red-500 bg-red-100 rounded-md w-60">
                    <p>Salari Max : {data?.salary_max}/Mount</p>   
                </span>
                <span className="flex items-center px-2 py-1 mt-2 text-sm font-semibold text-red-500 bg-red-100 rounded-md w-60">
                    <p>Salari Min : {data?.salary_min}/Mount</p>   
                </span>
              </div>
            </div>

    </div>

    
    
  )
}

export default Detail