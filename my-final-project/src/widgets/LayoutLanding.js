import React from 'react'
import Navigation from '../components/Navigation'
import Footer from '../components/Footer'

const LayoutLanding = (props) => {
  console.log(props)
  return (
    <>
      <Navigation/>
        <div className='container mx-auto my-10'>
          {props.children}
        </div>
      <Footer/>
    </>
  )
}

export default LayoutLanding