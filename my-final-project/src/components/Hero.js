import React from 'react'

const Hero = () => {
  return (
    <div className='container mt-10 mx-auto'>
        <h1 className='font-bold text-3xl'>Kunjungi Website Kami</h1>
        <p className='text-md'>Ada lebih dari 100 loker terbaru setiap harinya</p>
    </div>
  )
}

export default Hero