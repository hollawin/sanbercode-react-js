import React from 'react'
import { Navbar } from 'flowbite-react';
import { Link, useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';

const Navigation = () => {

  const navigate = useNavigate()
  const handleLogout = () => {
    Cookies.remove('token')
    navigate('/login')
  }
    
  return (
    <Navbar
  fluid
  rounded
>
<div className="container flex flex-wrap items-center justify-between mx-auto">
    <Link to="/" className="flex items-center">
                <img src="https://flowbite.com/docs/images/logo.svg" className="h-8 mr-3" alt="Flowbite Logo" />
                <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">siLOKER</span>
            </Link>
  <div className="flex md:order-2">
    <Navbar.Toggle />
  </div>
  <Navbar.Collapse>
    <ul>
        <li>
            <Link to="/" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                <span className="ml-3">Home</span>
            </Link>
        </li>
    </ul>
    {
        !Cookies.get('token') && <ul>
            <li>
                <Link to="/login" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                    <span className="ml-3">Login</span>
                </Link>
            </li>
        </ul>
    }
    {
        Cookies.get('token') && <>
            <ul>
                <li>
                    <Link to="/dashboard" className="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                        <span className="ml-3">Dashboard</span>
                    </Link>
                </li>
            </ul>
            <ul>
                <li>
                    <span onClick={handleLogout} className="flex items-center p-2 text-base font-normal text-red-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                        <span className="ml-3">Logout</span>
                    </span>
                </li>
            </ul>
        </>
    }
  </Navbar.Collapse>
  </div>
</Navbar>
  )
}

export default Navigation