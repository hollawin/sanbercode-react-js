import React from 'react'
import { Link } from 'react-router-dom'

const Sidebar = () => {
  return (
    <div className="relative hidden h-screen shadow-lg lg:block w-80">
    <div className="h-full bg-white dark:bg-gray-700">
      <div className="flex items-center justify-start pt-6 ml-8">
        <p className="text-xl font-bold dark:text-white">
          siLOKER
        </p>
      </div>
      <nav className="mt-6">
        <div>
        <Link to ={'/'} className="flex items-center justify-start w-full p-2 pl-6 my-2 text-gray-400 transition-colors duration-200 border-l-4 border-transparent hover:text-gray-800" >
            <span className="text-left">
              <svg width={20} height={20} fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1728 608v704q0 92-66 158t-158 66h-1216q-92 0-158-66t-66-158v-960q0-92 66-158t158-66h320q92 0 158 66t66 158v32h672q92 0 158 66t66 158z">
                </path>
              </svg>
            </span>
            <span className="mx-4 text-sm font-normal">
              Home
            </span>
          </Link>
          <Link to ={'/dashboard'} className="flex items-center justify-start w-full p-2 pl-6 my-2 text-gray-400 transition-colors duration-200 border-l-4 border-transparent hover:text-gray-800" >
            <span className="text-left">
              <svg width={20} height={20} fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1728 608v704q0 92-66 158t-158 66h-1216q-92 0-158-66t-66-158v-960q0-92 66-158t158-66h320q92 0 158 66t66 158v32h672q92 0 158 66t66 158z">
                </path>
              </svg>
            </span>
            <span className="mx-4 text-sm font-normal">
              Dashboard
            </span>
          </Link>
          <Link to ={'/dashboard/list-job-vacancy'} className="flex items-center justify-start w-full p-2 pl-6 my-2 text-gray-400 transition-colors duration-200 border-l-4 border-transparent hover:text-gray-800" >
            <span className="text-left">
              <svg width={20} height={20} fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1728 608v704q0 92-66 158t-158 66h-1216q-92 0-158-66t-66-158v-960q0-92 66-158t158-66h320q92 0 158 66t66 158v32h672q92 0 158 66t66 158z">
                </path>
              </svg>
            </span>
            <span className="mx-4 text-sm font-normal">
              List Data
            </span>
          </Link>
          <Link to ={'/dashboard/create-job-vacancy'} className="flex items-center justify-start w-full p-2 pl-6 my-2 text-gray-400 transition-colors duration-200 border-l-4 border-transparent hover:text-gray-800" >
            <span className="text-left">
              <svg width={20} height={20} fill="currentColor" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1728 608v704q0 92-66 158t-158 66h-1216q-92 0-158-66t-66-158v-960q0-92 66-158t158-66h320q92 0 158 66t66 158v32h672q92 0 158 66t66 158z">
                </path>
              </svg>
            </span>
            <span className="mx-4 text-sm font-normal">
              Create Data
            </span>
          </Link>
        </div>
      </nav>
    </div>
  </div>
  )
}

export default Sidebar