import React from "react";
import RouteComponent from "./routes/RouteComponent";

export const App = () => <RouteComponent />;

export default App;
