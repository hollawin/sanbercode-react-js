import { React, useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

const Navbar = () => {
  const { state } = useContext(GlobalContext);
  const { input } = state;

  return (
    <div>
      <nav className=" container w-4/5 mx-auto">
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl px-4 md:px-6 py-2.5">
          <h1 className="text-xl">Kelas ReactJS Batch 45</h1>

          <div className="flex items-center">
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas6"
            >
              Tugas 6
            </Link>
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas7"
            >
              Tugas 7
            </Link>
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas8"
            >
              Tugas 8
            </Link>
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas9"
            >
              Tugas 9
            </Link>
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas10"
            >
              Tugas 10
            </Link>
            <Link
              onClick={input}
              className="mx-1.5 hover:underline"
              to="/tugas11"
            >
              Tugas 11
            </Link>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
