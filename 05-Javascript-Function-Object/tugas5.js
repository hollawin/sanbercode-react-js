//Soal 1 - Membuat Function dengan return String
function nama() {
  return "Winda Widiawati";
}

var tampung = nama();
console.log("Hello Nama Saya " + tampung);

// Soal 2 - Membuat Function dengan rumus penjumlahan didalamnya)
let angka1 = 20;
let angka2 = 7;
function jumlahAngka() {
  return angka1 + angka2;
}

var tampung = jumlahAngka();
console.log(tampung);

//Soal 3 - Mengubah dalam bentuk arrow function
const hello = () => {
  return "Hello";
};
hello();

//Soal 4 - Memanggil key dalam sebuah object
let obj = {
  nama: "john",
  umur: 22,
  bahasa: "indonesia",
};
console.log(obj.bahasa);

//Soal 5 - Mengubah array menjadi object
let arrayDaftarPeserta = ["John Doe", "Laki-laki", "Baca Buku", 1992];
let objDaftarPeserta = {
  Nama: arrayDaftarPeserta[0],
  JenisKelamin: arrayDaftarPeserta[1],
  Hobby: arrayDaftarPeserta[2],
  TahunLahir: arrayDaftarPeserta[3],
};
console.log(objDaftarPeserta);

//Soal 6 - Membuat sebuah array of object dan melakukan filter
var buah = [
  { nama: "Nanas", warna: "Kuning", adaBijinya: "False", harga: 9000 },
  { nama: "Jeruk", warna: "Orang", adaBijinya: "True", harga: 8000 },
  {
    nama: "Semangka",
    warna: "Hijau dan Merah",
    adaBijinya: "True",
    harga: 10000,
  },
  { nama: "Pisang", warna: "Kuning", adaBijinya: "False", harga: 5000 },
];

var arrayBuahFilter = buah.filter(function (item) {
  return item.adaBijinya != "True";
});

console.log(arrayBuahFilter);

//Soal 7 - Destructuring pada Object
let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
};

const { name, brand, year } = phone;
console.log(name, brand, year);

//Soal 8 - Spred Operator pada Object
let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
};

let objOutput = {
  penulis: "john doe",
  tahunTerbit: 2020,
  nama: "pemograman dasar",
  jumlahHalaman: 172,
};

console.log(objOutput);

//soal 9 - Penggunaan Function dan Object
let mobil = {
  merk: "bmw",
  color: "red",
  year: 2002,
};

console.log(mobil);
