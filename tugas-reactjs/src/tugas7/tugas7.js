import React from "react";

function Tugas7(props) {
  return (
    <>
      <div class="container">
        <h1>Data Diri Peserta Kelas React JS</h1>
        <ul>
          <li>
            {" "}
            <strong>Nama : </strong> {props.name}{" "}
          </li>
          <li>
            {" "}
            <strong>Email : </strong> {props.email}{" "}
          </li>
          <li>
            {" "}
            <strong>Batch Pelatihan : </strong> {props.batch}{" "}
          </li>
        </ul>
      </div>
    </>
  );
}

export default Tugas7;
