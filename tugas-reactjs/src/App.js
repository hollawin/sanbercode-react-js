import React from "react";
import Tugas6 from "./tugas6/tugas6";
import Tugas7 from "./tugas7/tugas7";
import Tugas8 from "./tugas8/tugas8";
import "./App.css";

const App = () => {
  return (
    <>
      <Tugas6 />
      <Tugas7
        name="Winda Widiawati"
        batch="45"
        email="widiawatiwinda99@gmail.com"
      />
      <Tugas8 />
    </>
  );
};

export default App;
