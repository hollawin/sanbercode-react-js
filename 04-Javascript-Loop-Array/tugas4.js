//Soal 1 - Membuat Looping Sederhana
for (var angka = 0; angka <= 10; angka++) {
  console.log(angka);
}

//Soal 2 - Membuat Looping Dengan Conditional Angka Ganjil
for (var deret = 1; deret < 10; deret += 2) {
  console.log(deret);
}

//Soal 3 - Membuat Looping Dengan Conditional Angka Genap
for (var deret = 0; deret < 10; deret += 2) {
  console.log(deret);
}

//Soal 4 - Mengakses Element Array
let array1 = [1, 2, 3, 4, 5, 6];
console.log(array1[5]);

//Soal 5 - Mengurutkan Element Array
let array2 = [5, 2, 4, 1, 3, 5];
array2.sort();
console.log(array2);

//Soal 6 - Mengeluarkan Element Array
let array3 = [
  "selamat",
  "anda",
  "melakukan",
  "perulangan",
  "array",
  "dengan",
  "for",
];

for (var i = 0; i < array3.length; i++) {
  console.log(array3[i]);
}

//Soal 7 - Mengeluarkan Element Array Dengan Kondisi
for (var deret = 0; deret < 10; deret += 2) {
  console.log(deret);
}

//Soal 8 - Menggabungkan Element Menjadi String
let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];
var slug = kalimat.join(" ");
console.log(slug);

//Soal 9 - Menambahkan Element Array
var sayuran = [
  "Kangkung",
  "Bayam",
  "Buncis",
  "Kubis",
  "Timun",
  "Seledri",
  "Tauge",
];
sayuran.push();
console.log(sayuran);
